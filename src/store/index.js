import { createStore } from 'redux'

// tambah/modif initial state untuk app reactjs ini disini
const emptyUser = {
    fullname: "",
    email: "",
    isapproved: false,
    isadmin: false,
    token: "",
}

const initialState = {
    isLoggedIn: false,
    user: emptyUser
}

// berbagai action type yang tersedia di app ini:
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'LOGIN':
            return {
                ...state,
                isLoggedIn: true,
                user: {
                    fullname: action.payload.user.fullname,
                    email: action.payload.user.email,
                    isapproved: action.payload.user.isapproved,
                    isadmin: action.payload.user.isadmin,
                    token: action.payload.token,
                }
            }
        case 'LOGOUT':
            return {
                ...state,
                isLoggedIn: false,
                user: emptyUser
            }
        default:
            return state
    }
}

const store = createStore(
    reducer,
    // untuk debugger pada web browser
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export default store