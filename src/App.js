import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Register from './components/Registrasi/RegistrasiCard'
import RegisterView from './components/Registrasi/RegisterView'
import RegistrasiLevel1 from './components/Registrasi/RegistrasiLevel1'
import RegistrasiLevel2 from './components/Registrasi/RegistrasiLevel2'
import RegistrasiLevel3 from './components/Registrasi/RegistrasiLevel3'
import Profil from './components/Profile/ProfilCard'
import ProfilView from './components/Profile/ProfileView'

import Home from './components/home/Home'
import Login from './components/login/Login'
import Logout from './components/login/Logout'
import Konten from './components/navbar/navbar'
import NoMatch from './components/error/Error404'

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/register">
          <Register />
        </Route>
        <Route exact path="/registrasiview">
          <RegisterView />
        </Route>
        <Route exact path="/registrasilvl1">
          <RegistrasiLevel1 />
        </Route>
        <Route exact path="/regsitrasiLvl2">
          <RegistrasiLevel2 />
        </Route>
        <Route exact path="/regsitrasiLvl3">
          <RegistrasiLevel3 />
        </Route>

        <Route exact path="/profile">
          <Profil />
        </Route>
        <Route exact path="/profileview">
          <ProfilView />
        </Route>

        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/logout">
          <Logout />
        </Route>        
        <Route exact path="/">
          <Home />
        </Route>

        <Route exact path="/kontent">
          <Konten />
        </Route>
          
        <Route path='*'>
          <NoMatch />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;