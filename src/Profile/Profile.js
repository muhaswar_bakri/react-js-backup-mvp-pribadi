import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAddressBook, faHome, faIdCard, faPhone, faPhoneSquare, faTransgender, faTransgenderAlt, faUserCircle, faUserLock, faVenusMars } from '@fortawesome/free-solid-svg-icons';
import { faFacebookF, faGoogle, faTwitterSquare } from '@fortawesome/free-brands-svg-icons';
import './Profile.css';


export default class Profile extends Component {
    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <h2 className="fontku">Profile</h2>
                        <form>
                            <div className="form-group">
                                <label for="Identitas">
                                    <FontAwesomeIcon icon={faIdCard} /> No. Identitas 
                                </label>
                                <input type="email" className="form-control" placeholder="Masukan No. Identitas anda" required />
                            </div>
                            <div className="form-group">
                                <label for="Username">
                                    <FontAwesomeIcon icon={faUserCircle} /> Username 
                                </label>
                                <input type="email" className="form-control" placeholder="Masukan Username anda" required />
                            </div>
                            <div className="form-group">
                                <label for="telepon">
                                <FontAwesomeIcon icon={faPhone}/> No. Telepon </label>
                                <input type="email" className="form-control" placeholder="Masukan No. Telepon anda" required />
                            </div>

                            <label><FontAwesomeIcon icon={faVenusMars}/> Jenis Kelamin </label>
                            <div class="form-check">
                                <label class="form-check-label" for="radio1">
                                    <input type="radio" class="form-check-input" id="radio1" name="optradio" value="option1" checked />Laki Laki
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label" for="radio2">
                                    <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2" />Wanita
                                </label>
                            </div>
                            <br />
                            <div class="form-group">
                                <label for="comment"><FontAwesomeIcon icon={faHome}/> Alamat:</label>
                                <textarea class="form-control" rows="5" id="comment"></textarea>
                            </div>
                            <div className="form-group">
                                <label for="exampleInputPassword1"> 
                                    <FontAwesomeIcon icon={faUserLock} /> Password
                                </label>
                                <input type="password" className="form-control" placeholder="Password" />
                            </div>

                            <button type="submit" class="btn btn-primary btn-lg btn-block my-5"> Input </button>
                            
                        </form>

                    </div>
                </div>
            </div>
        );
    }
}