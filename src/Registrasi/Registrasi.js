import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAddressBook, faCoffee, faIdCard, fas, faUserLock } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faFacebookF, faGoogle, faInstagram, faTwitterSquare } from '@fortawesome/free-brands-svg-icons' ;
import styles from './Registrasi.css';

class Registrasi extends Component {

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <h2 className="fontku">Registrasi</h2>
                        <form>
                        <div className="form-group">
                                <label for="Identitas"><FontAwesomeIcon icon={faIdCard} /> No. Identitas </label>
                                <input type="email" className="form-control" placeholder="Masukan No. Identitas anda" required />
                            </div>
                            <div className="form-group">
                                <label for="Username"> <FontAwesomeIcon icon={faAddressBook}/> Username </label>
                                <input type="email" className="form-control" placeholder="Masukan username anda" required  />
                            </div>
                            <div className="form-group">
                                <label for="exampleInputPassword1"> <FontAwesomeIcon icon={faUserLock}/> Password</label>
                                <input type="password" className="form-control" placeholder="Password" />
                            </div>
                            
                            <button type="submit" class="btn btn-primary btn-lg btn-block my-5"> Input </button>
                            <h5 className="opsi">Atau Login menggunakan</h5>
                            <hr/>
                            <div className="row">
                                <div className="col-sm">
                                    <button type="submit" class="btn btn-danger btn-lg btn-block"> 
                                    <FontAwesomeIcon icon={faGoogle} /> Gmail</button>
                                </div>
                                <div className="col-sm">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block"> 
                                    <FontAwesomeIcon icon={faFacebookF}/> facebook</button>
                                </div>
                                <div className="col-sm">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block"> 
                                    <FontAwesomeIcon icon={faTwitterSquare}/> Instagram</button>
                                </div>
                            </div>
                        </form>
                         
                    </div>
                </div>
            </div>
        );
    }
}

export default Registrasi;