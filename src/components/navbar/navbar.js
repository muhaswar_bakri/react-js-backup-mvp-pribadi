import { faBell, faComment, faHome, faUserCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import navbarBrand from '../../assets/image/CC_1_1.png'
import inovatif1 from '../../assets/image/inovatif4.jpg'
import inovatif2 from '../../assets/image/inovatif5.png'
import inovatif3 from '../../assets/image/inovatif6.jpg'
import profil from '../../assets/image/profile.png'
import ps5 from '../../assets/image/ps5.jpeg'
import rserver from '../../assets/image/ruangan_server.jpg'
import ai from '../../assets/image/ai.jpeg'
import './navbar.css'

export default class NavbarItem extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                    <a className="navbar-brand mx-2" href="/">
                        <img src={navbarBrand} alt="logo-cocreate" />
                    </a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">

                            <form className="form-inline my-2 my-lg-0">
                                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                                <button className="btn btn-outline-danger my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </ul>

                        <Link to="/"><FontAwesomeIcon className="iconWidth" icon={faHome}></FontAwesomeIcon></Link>
                        <Link to="/registrasiview"><FontAwesomeIcon className="iconWidth" icon={faBell}></FontAwesomeIcon></Link>
                        <Link to="/profileview"><FontAwesomeIcon className="iconWidth" icon={faUserCircle}></FontAwesomeIcon></Link>
                    </div>
                </nav>

                {/*---------- CAROUSEL -----------*/}
                <div className="container containerHeight">
                    <div className="carouselku">
                        <div className="row">
                            <di className="col">
                                <div id="carouselExampleCaptions" className="carousel slide" data-ride="carousel">
                                    <ol className="carousel-indicators">
                                        <li data-target="#carouselExampleCaptions" data-slide-to="0" className="active"></li>
                                        <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                                        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                                    </ol>
                                    <div className="carousel-inner">
                                        <div className="carousel-item active">
                                            <img src={inovatif1} className="d-block w-100 carouselWidth" alt="..." />
                                            <div className="carousel-caption d-none d-md-block">
                                                <h5>First slide label</h5>
                                                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                                            </div>
                                        </div>
                                        <div className="carousel-item">
                                            <img src={inovatif2} className="d-block w-100 carouselWidth" alt="..." />
                                            <div className="carousel-caption d-none d-md-block">
                                                <h5>Second slide label</h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            </div>
                                        </div>
                                        <div className="carousel-item">
                                            <img src={inovatif3} className="d-block w-100 carouselWidth" alt="..." />
                                            <div className="carousel-caption d-none d-md-block">
                                                <h5>Third slide label</h5>
                                                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <a className="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span className="sr-only">Previous</span>
                                    </a>
                                    <a className="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span className="sr-only">Next</span>
                                    </a>
                                </div>
                            </di>
                        </div>
                    </div>


                    {/* ----------  TRENDING PROYEK----------*/}
                    <div>
                        <button className="btn btn-outline-warning text-dark trendingTextBtn" disabled>Trending</button>
                        <div className="row">
                            <div className="col">
                                <div className="card cardTrending">
                                    <img src={profil} className="cardImg" alt="..." />
                                    <div className="card-body cardBodyWidth">
                                        <h2 className="display-5 text-center">Smart Home </h2>
                                        <hr />
                                        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <p><a href="#">Selengkapnya...</a></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="card cardTrending">
                                    <img src={profil} className="cardImg" alt="..." />
                                    <div className="card-body cardBodyWidth">
                                        <h2 className="display-5 text-center">Inovation </h2>
                                        <hr />
                                        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <p><a href="#">Selengkapnya...</a></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="card cardTrending">
                                    <img src={profil} className="cardImg" alt="..." />
                                    <div className="card-body cardBodyWidth">
                                        <h2 className="display-5 text-center">Ideas </h2>
                                        <hr />
                                        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <p><a href="#">Selengkapnya...</a></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="card cardTrending">
                                    <img src={profil} className="cardImg" alt="..." />
                                    <div className="card-body cardBodyWidth">
                                        <h2 className="display-5 text-center">Financial </h2>
                                        <hr />
                                        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <p><a href="#">Selengkapnya...</a></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="card cardTrending">
                                    <img src={profil} className="cardImg" alt="..." />
                                    <div className="card-body cardBodyWidth">
                                        <h2 className="display-5 text-center">Financial </h2>
                                        <hr />
                                        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <p><a href="#">Selengkapnya...</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />

                    {/* ---------ARTIKEL--------- */}
                    <div>
                        <button className="btn btn-outline-danger text-dark trendingTextBtn" disabled>Artikel</button>
                        <div className="row">
                            <div className="col">
                                <div className="card cardArtikel">
                                    <img src={ps5} className="cardImgArtikel" alt="..." />
                                    <div className="card-body cardBodyWidthHeight">
                                        <h2 className="display-5 text-center">PS5 Ternyata Masih Mengalami Bug</h2>
                                        <hr />
                                        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <p className="pengirim"> <strong>Aisyah Nurul H </strong> - <em>45 menit lalu</em></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="card cardArtikel">
                                    <img src={ai} className="cardImgArtikel" alt="..." />
                                    <div className="card-body cardBodyWidthHeight">
                                        <h2 className="display-5 text-center">Manfaat AI pada Pemerintah</h2>
                                        <hr />
                                        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <p className="pengirim"> <strong>Gotfried </strong> - <em>1 menit lalu</em></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col">
                                <div className="card cardArtikel">
                                    <img src={rserver} className="cardImgArtikel" alt="..." />
                                    <div className="card-body cardBodyWidthHeight">
                                        <h2 className="display-5 text-center">Menata Ruang Server bagi Perusahaan</h2>
                                        <hr />
                                        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <p className="pengirim"> <strong>Apriansyah </strong> - <em>23 menit lalu</em></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/*------------ FORUM --------- */}
                    <div>
                    <button className="btn btn-outline-success text-dark trendingTextBtn" disabled>Forum</button>
                        <div className="row my-3">
                            <div className="col-md-1">
                                <img src={ps5} className="forumImg" />
                            </div>
                            <div className="col-md-6">
                                <p className="textForum">Design Studio & Apple</p>
                                <p className="pengirim">Aisyah <em>23 menit lalu</em></p>
                            </div>
                            <div className="col-md-4 text-right">
                                <button className="btn btn-danger btnForum">Teknologi Informasi-IT </button>
                            </div>
                            <div className="col-md-1">
                                <p className="textIconForum"><FontAwesomeIcon icon={faComment}></FontAwesomeIcon>32 </p>
                            </div>
                        </div>
                        <div className="row my-3">
                            <div className="col-md-1">
                                <img src={profil} className="forumImg" />
                            </div>
                            <div className="col-md-6">
                                <p className="textForum">Design Studio & Apple</p>
                                <p className="pengirim">Aisyah <em>23 menit lalu</em></p>
                            </div>
                            <div className="col-md-4 text-right">
                                <button className="btn btn-danger btnForum">Teknologi Informasi-IT </button>
                            </div>
                            <div className="col-md-1">
                                <p className="textIconForum"><FontAwesomeIcon icon={faComment}></FontAwesomeIcon>32 </p>
                            </div>
                        </div>
                        <div className="row my-3">
                            <div className="col-md-1">
                                <img src={ai} className="forumImg" />
                            </div>
                            <div className="col-md-6">
                                <p className="textForum">Design Studio & Apple</p>
                                <p className="pengirim">Aisyah <em>23 menit lalu</em></p>
                            </div>
                            <div className="col-md-4 text-right">
                                <button className="btn btn-danger btnForum">Teknologi Informasi-IT </button>
                            </div>
                            <div className="col-md-1">
                                <p className="textIconForum"><FontAwesomeIcon icon={faComment}></FontAwesomeIcon>32 </p>
                            </div>
                        </div>
                        <div className="row my-3">
                            <div className="col-md-1">
                                <img src={profil} className="forumImg" />
                            </div>
                            <div className="col-md-6">
                                <p className="textForum">Design Studio & Apple</p>
                                <p className="pengirim">Aisyah <em>23 menit lalu</em></p>
                            </div>
                            <div className="col-md-4 text-right">
                                <button className="btn btn-danger btnForum">Teknologi Informasi-IT </button>
                            </div>
                            <div className="col-md-1">
                                <p className="textIconForum"><FontAwesomeIcon icon={faComment}></FontAwesomeIcon>32 </p>
                            </div>
                        </div>
                        <div className="row my-3">
                            <div className="col-md-1">
                                <img src={ai} className="forumImg" />
                            </div>
                            <div className="col-md-6">
                                <p className="textForum">Design Studio & Apple</p>
                                <p className="pengirim">Aisyah <em>23 menit lalu</em></p>
                            </div>
                            <div className="col-md-4 text-right">
                                <button className="btn btn-danger btnForum">Teknologi Informasi-IT </button>
                            </div>
                            <div className="col-md-1">
                                <p className="textIconForum"><FontAwesomeIcon icon={faComment}></FontAwesomeIcon>32 </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}