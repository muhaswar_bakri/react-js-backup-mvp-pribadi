import React, { Component } from 'react'
import { ProgressBar } from 'react-bootstrap'
import selesai from '../../assets/image/registrasiSelesai.png'
import './Registrasi.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
export default class RegistrasiLevel3 extends Component {
    render() {
        const percentage = 100
        return (
            <div className="container my-5">
                <div className="progressBar my-3">
                    <ProgressBar striped variant="danger" now={percentage}
                        label={`${percentage}%Completed`} />
                </div>
                <div className="row md-12 my-4">
                    <div className="col">
                        <img className="img-fluid" src={selesai} />
                    </div>
                    <div className="col my-5">
                        <h2 className="textEnd">Terima kasih !</h2>
                        <p className="textNote"> Registrasi anda akan segera di proses oleh admin. jangan lupa verifikasi Email ya...</p>
                        <div className="col-sm-12 text-right">
                            <button type="submit" className="btn btn-secondary mx-2"> <Link className="btnKembaliRegistrasi" to="/regsitrasiLvl2">Kembali</Link></button>
                            <button className="btn btn-danger text-right my-4"> <Link className="btnKembaliRegistrasi" to="/kontent"> Selesai </Link></button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}