import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAddressBook, faAt, faCoffee, faIdCard, fas, faUserLock } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faFacebookF, faGoogle, faGoogleDrive, faGooglePay, faGooglePlus, faGoogleWallet, faInstagram, faTwitterSquare } from '@fortawesome/free-brands-svg-icons';

class RegisterModal extends Component{
    render(){
        return(
            <div>
                <h5 className="content">Sudah Punya akun ?
                        <a type="submit" class="" data-toggle="modal" data-target="#exampleModalCenter">
                            Ganti Password
                            </a>
                        </h5>
                        

                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Ganti Password</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="Password" class="col-form-label">Password</label>
                                                <input type="text" class="form-control" id="Password" />
                                            </div>
                                            <div class="form-group">
                                                <label for="Password Baru" class="col-form-label">Password Baru</label>
                                                <input type="text" class="form-control" id="Password Baru" />
                                            </div>
                                            <div class="form-group">
                                                <label for="Ulangi Password" class="col-form-label">Ulangi Password</label>
                                                <input type="text" class="form-control" id="Ulangi Password" />
                                            </div>

                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
        );
    }
}

export default RegisterModal;