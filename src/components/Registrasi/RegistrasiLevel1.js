import React, { Component } from 'react'
import { ProgressBar } from 'react-bootstrap'
import kategori from '../../assets/image/registrasiKategori.png'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';

export default class RegistrasiLevel1 extends Component {
    render() {
        const percentage = 25
        return (
            <div className="container mt-5">
                <div className="progressBar my-3">
                    <ProgressBar striped variant="danger" now={percentage}
                        label={`${percentage}%Completed`} />
                </div>

                <div className="row">
                    <div className="col md-2">
                        <img className="contentimg" src={kategori} />
                    </div>
                    <div className="col md-10 mt-4">
                        <div>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2"> Keuangan</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2"> Otomotif</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2"> Teknologi Informasi / IT</button>
                        </div>
                        <br/>
                        <div>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2"> Sosial</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2"> Olahraga</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2"> Sains</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2"> Desain</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 mt-2"> Bisnis dan Manajemen</button>
                        </div>
                        <br/>
                        <div>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2"> Kewirausahan</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2"> Kesehatan</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2"> Energi dan Lingkungan</button>
                            <button className="btn btn-light bg-white text-dark border border-danger rounded mx-2 mt-2"> Hukum dan Advokasi</button>
                        </div>
                        <button className="btn btn-secondary mt-5 mx-2"><Link className="btnKembaliRegistrasi" to="/kontent"> Kembali </Link></button>
                        <button className="btn btn-danger mt-5"><Link className="btnKembaliRegistrasi" to="/regsitrasiLvl2"> Selanjutnya </Link></button>
                    </div>
                </div>
            </div>
        );
    }
}