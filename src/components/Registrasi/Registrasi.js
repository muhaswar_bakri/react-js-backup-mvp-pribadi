import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAddressBook, faAt, faCoffee, faIdCard, fas, faUserLock } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faFacebookF, faGoogle, faGoogleDrive, faGooglePay, faGooglePlus, faGoogleWallet, faInstagram, faTwitterSquare } from '@fortawesome/free-brands-svg-icons';
import styles from './Registrasi.css';

class Registrasi extends Component {
    
    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <h2 className="text-center my-5">CoCreate Akun</h2>
                        <h5 className="content">Sudah Punya akun ?
                        <a type="submit" class="" data-toggle="modal" data-target="#exampleModalCenter">Buat Akun</a>
                        </h5>
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Daftar</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div className="form-group">
                                                <label for="Username"> <FontAwesomeIcon icon={faAt} /> Email </label>
                                                <input type="email" className="form-control" placeholder="Masukan Email anda" required />
                                            </div>
                                            <div className="form-group">
                                                <label for="InputPassword"> <FontAwesomeIcon icon={faUserLock} /> Password</label>
                                                <input type="password" className="form-control" placeholder="Masukkan Password" />
                                            </div>
                                            <div className="form-group">
                                                <label for="UlangiInputPassword"> <FontAwesomeIcon icon={faUserLock} /> Ulangi Password</label>
                                                <input type="password" className="form-control" placeholder="Masukkan Password" />
                                            </div>
                                            <button type="submit" class="btn btn-danger btn-lg btn-block my-5"> Masuk</button>

                                            <h2 className="opsi"><span>Atau Login menggunakan</span></h2>
                                            <div className="row my-4">
                                            <div >
                                                <button type="submit" class="btn btn-outline-primary mx-1">
                                                    <FontAwesomeIcon icon={faFacebookF} /> facebook</button>
                                            </div>
                                            <div>
                                                <button type="submit" class="btn btn-outline-danger">
                                                    <FontAwesomeIcon icon={faGoogle} /> Gmail</button>
                                            </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr />
                        <br></br>

                        <form>
                            <div className="form-group my-5">
                                <label for="Username"> <FontAwesomeIcon icon={faAt} /> Email </label>
                                <input type="email" className="form-control" placeholder="Masukan Email anda" required />
                            </div>
                            <div className="form-group">
                                <label for="exampleInputPassword1"> <FontAwesomeIcon icon={faUserLock} /> Password</label>
                                <input type="password" className="form-control" placeholder="Password" />
                            </div>
                            <button type="submit" class="btn btn-danger btn-lg btn-block my-5"> Masuk </button>

                            <h2 className="opsi"><span>Atau Login menggunakan</span></h2>
                            <div className="row my-4">
                            <div >
                                <button type="submit" class="btn btn-outline-primary mx-1">
                                    <FontAwesomeIcon icon={faFacebookF} /> facebook</button>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-outline-danger">
                                    <FontAwesomeIcon icon={faGoogle} /> Gmail</button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Registrasi;