import { faBan, faCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react';
import Navbar2 from '../Navbar2/navbar2'
export default class RegisterView extends Component {
    constructor() {
        super();
        this.state = {
            person: []
        }
        this._approveHandle = this._approveHandle.bind(this)
    }
    componentDidMount() {
        var axios = require('axios');

        var config = {
            method: 'get',
            url: 'http://kelompok5.dtstakelompok1.com/admin/account',
            headers: {
                'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJ1c2VyQGdtYWlsLmNvbSJ9.7cRfP6ACg_Y3-0cxgqTk8S33zlcsqVL9PTWjBPF53pk'
            }
        };

        axios(config)
            .then((response) => {
                const person = response.data.account
                console.log("data api", person)
                this.setState({ person })
                // console.log(JSON.stringify(response.data));
                console.log(person.email)

            })
            .catch(function (error) {
                console.log("Ini errornya", error);
            });
    }

    _approveHandle() {
        var axios = require('axios');

        var config = {
            method: 'post',
            url: 'http://kelompok5.dtstakelompok1.com/admin/account/8',
            headers: {
                'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlcm5hbWUiOiJhZG1pbkBnbWFpbC5jb20ifQ.WtxAH0qixSPf2Q_moYx1v4vIlSwYZIh1aMDFAgn5Q38'
            }
        };

        axios(config)
            .then(function (response) {
                console.log(JSON.stringify(response.data));
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    _deletHandle() {
        var axios = require('axios');

        var config = {
            method: 'post',
            url: 'http://kelompok5.dtstakelompok1.com/admin/account/3/delete',
            headers: {
                'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlcm5hbWUiOiJhZG1pbkBnbWFpbC5jb20ifQ.WtxAH0qixSPf2Q_moYx1v4vIlSwYZIh1aMDFAgn5Q38'
            }
        };

        axios(config)
            .then(function (response) {
                console.log(JSON.stringify(response.data));
            })
            .catch(function (error) {
                console.log(error);
            });

    }
    render() {
        // const {person} = this.state
        return (
            <div className="container">
                <Navbar2 />
                <ul>
                    {/* {this.state.person.map(person => {
                        return (
                            <div>{person.email}</div>
                        )
                    })} */}
                </ul>
                <div className="card">
                    <div className="card-header">Daftar Register User</div>
                    <div className="card-body">
                        <div className="table-responsive">
                            <pre>ini data</pre>
                            <table className="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Aktivasi</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {this.state.person.map(person => {
                                        return (
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>{person.email}</td>
                                                <td>{person.name}</td>
                                                <td>{person.isapproved ? "Aktive" : "Tolak"}</td>
                                                <td>
                                                    <button className="btn btn-primary mx-2" 
                                                    onClick={this._approveHandle}>
                                                        <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon> Aktif</button>
                                                    <button className="btn btn-danger" onClick={this._deletHandle}>
                                                        <FontAwesomeIcon icon={faBan}></FontAwesomeIcon> Tolak</button>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}