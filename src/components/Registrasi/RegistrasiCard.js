import React, { Component } from 'react';

import './Registrasi.css';
import profile from '../../assets/image/registrasi.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF, faGoogle } from '@fortawesome/free-brands-svg-icons';
import { faAt, faUser, faUserLock } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios'
import { Link, Redirect } from 'react-router-dom';
class RegistrasiCard extends Component {
    constructor() {
        super();
        this.state = {
            usernameRegis: '',
            emailRegis: '',
            passwordRegis: '',
            redirect: false,
            users: []
        }

        this._handleUserName = this._handleUserName.bind(this)
        this._handleEmailChange = this._handleEmailChange.bind(this)
        this._handlePasswordChange = this._handlePasswordChange.bind(this)
        this.savedataToAPI = this.savedataToAPI.bind(this)
        this._googleAPI = this._googleAPI.bind(this)

    }
    _handleUserName(event) {
        this.setState({
            usernameRegis: event.target.value
        })
    }

    _handleEmailChange(event) {
        this.setState({
            emailRegis: event.target.value
        })
    }

    _handlePasswordChange(event) {
        this.setState({
            passwordRegis: event.target.value
        })
    }

    savedataToAPI(event) {
        // debugger
        event.preventDefault()
        const url = "http://kelompok5.dtstakelompok1.com/register"
        const dataRegis = {
            name: this.state.usernameRegis,
            email: this.state.emailRegis,
            password: this.state.passwordRegis
        }
        axios.post(url, dataRegis)
            .then((res) => {
                console.log("data api", res)
                alert("Data sudah di kirim")
                this.setState({
                    redirect: true
                })

            }).catch((err) => {
                console.log("error kirim data", err)
            })
        console.log("Regis", dataRegis)
    }

    _googleAPI(event) {
        event.preventDefault()
        var config = {
            method: 'get',
            url: 'http://kelompok5.dtstakelompok1.com/login/google',
            headers: {}
        };

        axios(config)
            .then(function (response) {
                console.log(JSON.stringify("respone googel",response.data));
            })
            .catch(function (error) {
                console.log("gagal oten",error);

            });
    }


    render() {
        if (this.state.redirect) {
            return <Redirect to="/" />
        }
        return (
            <div className="container spasiAtas">
                <pre> debug usernamer : {this.state.usernameRegis} </pre>
                <pre> debug email : {this.state.emailRegis} </pre>
                <pre> debug password : {this.state.passwordRegis} </pre>
                <div className="row justify-content-center my-3">
                    <div className="col col-sm-8 my-3">
                        <img className="imgregis" src={profile} alt="profile" />
                    </div>
                    <div className="col col-sm-4">
                        <div className="card bg-light cardContainer">
                            <div className="card-header">Daftar</div>
                            <div className="card-body">
                                <form method="post" onSubmit={(event) => this.savedataToAPI(event)}>
                                    <div className="form-group">
                                        <label for="UsernameRegis">
                                            <FontAwesomeIcon icon={faUser}></FontAwesomeIcon> Nama Lengkap
                                            </label>
                                        <input type="text" id="usernameRegis" onChange={this._handleUserName} className="form-control" placeholder="Masukan Nama Lengkap anda" required />
                                    </div>
                                    <div className="form-group">
                                        <label for="Username">
                                            <FontAwesomeIcon icon={faAt}></FontAwesomeIcon> Email
                                            </label>
                                        <input type="email" id="emailRegis" onChange={this._handleEmailChange} className="form-control" placeholder="Masukan Email anda" required />
                                    </div>
                                    <div className="form-group">
                                        <label for="Password">
                                            <FontAwesomeIcon icon={faUserLock}></FontAwesomeIcon> Password
                                            </label>
                                        <input type="text" id="passwordRegis" onChange={this._handlePasswordChange} className="form-control" placeholder="Password anda" required />
                                    </div>

                                    <div className="form-group saveContainer">
                                        <button type="submit" id="btn-regis" className="btn btn-danger" data-toggle="modal" data-target="#exampleModalCenter">
                                            Daftar
                                            </button>

                                    </div>
                                    <h2 className="opsi texth2"><span className="textspan">Atau</span></h2>
                                    <br />
                                    <h3>Daftar akun dengan</h3>
                                    <div className="row mt-4 mb-1">
                                        <div >
                                            <button type="submit" className="btn btn-outline-primary mx-1">
                                                <FontAwesomeIcon icon={faFacebookF}></FontAwesomeIcon> facebook</button>
                                        </div>
                                        <div>
                                            <button type="submit" className="btn btn-outline-danger" 
                                            onClick={ (event) => this._googleAPI (event)}
                                            >
                                            <FontAwesomeIcon icon={faGoogle}></FontAwesomeIcon> Gmail</button>
                                        </div>
                                    </div>
                                    <p>Sudah punya akun ?
                                        <a className="text-danger"><Link to="/login">  Login </Link></a></p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default RegistrasiCard;