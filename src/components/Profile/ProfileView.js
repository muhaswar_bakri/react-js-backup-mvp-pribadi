import React, { Component } from 'react';
import './ProfileView.css'
import profileimg from '../../assets/image/profile.png'
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import Navbar2 from '../Navbar2/navbar2'
export default class ProfileView extends Component {
    render() {
        return (
            <div className="container">
                <Navbar2/>
                <div className="row">
                    <div className="col-md">
                        <div className="card">
                            <div className="card-header">
                                Profile User
                    </div>
                            <div className="card-body">
                                <div className="cardView">
                                    <img src={profileimg} alt="John" className="ukuran" />
                                    <h1>John Doe</h1>
                                    <p className="titleView">CEO & Founder, Example</p>
                                    <p>Harvard University</p>
                                    <a href="#"><i className="fa fa-dribbble"></i></a>
                                    <a href="#"><i className="fa fa-twitter"></i></a>
                                    <a href="#"><i className="fa fa-linkedin"></i></a>
                                    <a href="#"><i className="fa fa-facebook"></i></a>
                                </div>
                
                            </div>
                        </div>
                    </div>
                    <div className="col-md">
                        <div className="card-body">
                            <h5 className="card-title">Biodata User</h5>
                            <hr/>
                            <table className="table table-borderless">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col"></th>
                                        <th scope="col"></th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row"></th>
                                        <td>Nama</td>
                                        <td></td>
                                        <td>Marques</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"></th>
                                        <td>Jenis Kelamin</td>
                                        <td></td>
                                        <td>Laki Laki</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"></th>
                                        <td>Tanggal Lahir</td>
                                        <td></td>
                                        <td>1-09-1298</td>
                                    </tr>
                                    
                                    <tr>
                                        <th scope="row"></th>
                                        <td>Alamat</td>
                                        <td></td>
                                        <td>Jl. Anu kab. itu, kec.disana sedikit, desa jauh jauh lagi</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"></th>
                                        <td>No. Telepon</td>
                                        <td></td>
                                        <td>0897343471678</td>
                                    </tr>
                                </tbody>
                            </table>
                            <br/>
                            <button className="btn btn-primary"><Link className="profileBtnEdit" to="/profile"> + Edit data</Link></button>
                            <hr/>
                        </div>
                    </div>
                </div>
                <button className="btn btn-secondary"><Link className="profileBtnEdit" to="/kontent"><FontAwesomeIcon icon={faHome}></FontAwesomeIcon>Home </Link></button>
            </div>
        )
    }
}