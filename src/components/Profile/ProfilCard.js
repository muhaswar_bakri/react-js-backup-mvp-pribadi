import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAddressBook, faHome, faIdCard, faPhone, faPhoneSquare, faTransgender, faTransgenderAlt, faUserCircle, faUserLock, faVenusMars } from '@fortawesome/free-solid-svg-icons';
import { faFacebookF, faGoogle, faTwitterSquare } from '@fortawesome/free-brands-svg-icons';
import './Profile.css';
import profile from '../../assets/image/registrasi.png'
import { Link } from 'react-router-dom';


export default class ProfileCard extends Component {
    render() {
        return (
            <div className="spasiAtas">
                <div className="row justify-content-center">
                <div className="imgcontainer">
                    <img className="contentimg" src={profile}/>
                    </div>
                        <div className="card bg-light my-3 ">
                            <div className="card-header formku">Profil anda</div>
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label for="Identitas">
                                            <FontAwesomeIcon icon={faIdCard} /> No. Identitas
                                        </label>
                                        <input type="email" className="form-control" placeholder="Masukan No. Identitas anda"/>
                                    </div>
                                    <div className="form-group">
                                        <label for="Username">
                                            <FontAwesomeIcon icon={faUserCircle} /> Username
                                        </label>
                                        <input type="email" className="form-control" placeholder="Masukan Username anda"/>
                                    </div>
                                    <div className="form-group">
                                        <label for="telepon">
                                            <FontAwesomeIcon icon={faPhone} /> No. Telepon </label>
                                        <input type="email" className="form-control" placeholder="Masukan No. Telepon anda"/>
                                    </div>

                                    <label><FontAwesomeIcon icon={faVenusMars} /> Jenis Kelamin </label>
                                    <div className="form-check">
                                        <label className="form-check-label" for="radio1">
                                            <input type="radio" className="form-check-input" id="radio1" name="optradio" value="option1" checked />Laki Laki
                                </label>
                                    </div>
                                    <div className="form-check">
                                        <label className="form-check-label" for="radio2">
                                            <input type="radio" className="form-check-input" id="radio2" name="optradio" value="option2" />Wanita
                                        </label>
                                    </div>
                                    <br />
                                    <div className="form-group">
                                        <label for="comment"><FontAwesomeIcon icon={faHome} /> Alamat:</label>
                                        <textarea className="form-control" rows="5" id="comment"></textarea>
                                    </div>
                                    <div classNameName="form-group">
                                        <button type="button" className="btn btn-dark" data-toggle="modal" data-target="#exampleModalCenter">
                                            <FontAwesomeIcon icon={faUserLock}></FontAwesomeIcon>  Ganti Password
                                        </button>

                                        <div className="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div className="modal-dialog modal-dialog-centered" role="document">
                                                <div className="modal-content">
                                                    <div className="modal-header">
                                                        <h5 className="modal-title" id="exampleModalLongTitle">Ganti Password</h5>
                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div className="modal-body">
                                                        <form>
                                                            <div className="form-group">
                                                                <label for="Password" className="col-form-label">Password</label>
                                                                <input type="text" className="form-control" id="Password" />
                                                            </div>
                                                            <div className="form-group">
                                                                <label for="Password Baru" className="col-form-label">Password Baru</label>
                                                                <input type="text" className="form-control" id="Password Baru" />
                                                            </div>
                                                            <div className="form-group">
                                                                <label for="Ulangi Password" className="col-form-label">Ulangi Password</label>
                                                                <input type="text" className="form-control" id="Ulangi Password" />
                                                            </div>

                                                        </form>
                                                    </div>
                                                    <div className="modal-footer">
                                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Keluar</button>
                                                        <button type="button" className="btn btn-primary">Simpan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" className="btn btn-danger btn-lg btn-block my-5"> <Link className="ProfilBtn" to="/kontent"> Simpan </Link></button>

                                </form>
                            </div>
                        </div>


                    </div>
                </div>
        );
    }
}