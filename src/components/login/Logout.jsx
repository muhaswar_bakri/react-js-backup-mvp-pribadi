import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'

class Logout extends Component {
    render() {
        if (this.props.isLoggedIn) {
            this.props.dispatch({
                type: 'LOGOUT'
            })
            console.log('logging out...')
        }
        return (
            <Redirect to='/' />
        )
    }
}

function mapStateToProps(state) {
    return {
        isLoggedIn: state.isLoggedIn
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatch
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout)