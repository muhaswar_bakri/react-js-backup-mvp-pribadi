import react, { Component } from 'react'
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBell, faHome, faUserCircle } from '@fortawesome/free-solid-svg-icons'; 
import navbarBrand from '../../assets/image/CC_1_1.png'
export default class Navbar2 extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                    <a className="navbar-brand mx-2" href="/">
                        <img src={navbarBrand} alt="logo-cocreate" />
                    </a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">

                            <form className="form-inline my-2 my-lg-0">
                                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                                <button className="btn btn-outline-danger my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </ul>

                        <Link to="/"><FontAwesomeIcon className="iconWidth" icon={faHome}></FontAwesomeIcon></Link>
                        <Link to="/registrasiview"><FontAwesomeIcon className="iconWidth" icon={faBell}></FontAwesomeIcon></Link>
                        <Link to="/profileview"><FontAwesomeIcon className="iconWidth" icon={faUserCircle}></FontAwesomeIcon></Link>
                    </div>
                </nav>
            </div>
        )
    }
}