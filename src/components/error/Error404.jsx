import React, { Component } from 'react'
import "./Error404.css"

export default class Error404 extends Component {
    render() {
        return (
            <div className="container m-auto text-center error">
                <h2 className="font-weight-bold">Page Not Found</h2>
                <p>Laman yang kamu cari tidak ditemukan</p>
                <a href='/' role="button" className="btn btn-small">Home</a>
            </div>
        )
    }
}
