import React, { Component } from 'react'
import brand from '../../assets/image/CC_1_1.png'
import Flat_Design_Home from '../../assets/image/flat-design-home.png'
import { Link } from "react-router-dom";
import './Home.css'
import { connect } from 'react-redux';

class Home extends Component {
    render() {
        var userLoginMenu = (
            <li className="nav-item active">
                <Link className="nav-link btn btn-costum" to="/login">Login</Link>
            </li>
        )
        if (this.props.isLoggedIn) {
            userLoginMenu = (
                <li className="nav-item active">
                    <Link className="nav-link btn btn-costum" to="/logout">Logout</Link>
                </li>
            )
        }
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-costum">
                    <a className="navbar-brand" href="#"><img src={brand} /></a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                        </ul>

                        <ul className="navbar-nav">
                            {userLoginMenu}
                        </ul>


                    </div>
                </nav>

                <div className="container-fluid">
                    <div className="row">
                        <div className="col col-judul" style={{ backgroundColor: "#ffe1e0" }}>
                            <h1>Kolaborasi dan inovasi menjadi satu</h1>
                            <h3>Gabung dan jadilah bagian dari komunitas terbesar ini</h3>
                            <Link className="nav-link btn btn-register-home" to="/register">Get Started</Link>
                        </div>
                        <div className="col" style={{ padding: 10, backgroundColor: "#ffe1e0" }}>
                            <img src={Flat_Design_Home} width="700" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        isLoggedIn: state.isLoggedIn
    }
}

export default connect(mapStateToProps, null)(Home)